//JS

//Lazy-Load
var lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazyload"
});

//Menu
$(function () {
    $('.hamburger').on('click', function () {
        $('.toggle').toggleClass('open');
        $('.menu').toggleClass('open');
    });
});

//Menu-Scroll
$(window).scroll(function() {
    if($(document).scrollTop() > 0) {
        $('#header').addClass('fixed');
    } else {
        $('#header').removeClass('fixed')
    }
});

//Home-Why
$('.why-link').magnificPopup({
    type: 'iframe'
    // other options
});

//Home-Clients-Owl
$('.home-clients-owl').owlCarousel({
    loop:true,
    margin:25,
    nav:false,
    dots:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        },
        1200:{
            items:3
        },
        1400:{
            items:3
        },
        1600:{
            items:3
        }
    }
});

//Home-Brands-Owl
$('.home-brands-owl').owlCarousel({
    loop:true,
    margin:60,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:2
        },
        700:{
            items:3
        },
        1000:{
            items:3
        },
        1200:{
            items:4
        },
        1400:{
            items:5
        },
        1600:{
            items:6
        }
    }
});

//About-Clients-Owl
$('.about-clients-owl').owlCarousel({
    loop:true,
    margin:30,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:1
        },
        700:{
            items:1
        },
        1000:{
            items:1
        },
        1200:{
            items:1
        },
        1400:{
            items:1
        },
        1600:{
            items:1
        }
    }
});

//Portfolio-Tab
$(".portfolio-tab").pTab({
    pTab: '.tab-list',
    pTabElem: 'li',
    pContent: '.tab-content',
    pClass : 'aktif',
    pDuration : 500,
    pEffect : 'show',
});